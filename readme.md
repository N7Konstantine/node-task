# Node Task

This project contains a NodeJS hello-world application.

## Installation steps
1. Copy the repository
2. Install dependencies
3. Start the app

## Terminal commands

Code block:

```
git clone https://gitlab.com/N7Konstantine/node-task
cd ./node-task
npm i
```

> Start cmd: `npm run start`

## App should work
![N7 Wallpaper](N7wallpaper.png)
## TODO
- [x] Git Ignore
- [ ] Software Test
- [ ] Code Comments